const wordList = [
    {
        eng: "butter",
        rus: "масло"
    },
    {
        eng: "freeze",
        rus: "морозить"
    },
    {
        eng: "fall",
        rus: "падать"
    },
    {
        eng: "tube",
        rus: "труба"
    },
    {
        eng: "down",
        rus: "вниз"
    },
    {
        eng: "trust",
        rus: "верить"
    },
    {
        eng: "cake",
        rus: "пирог"
    },
    {
        eng: "tower",
        rus: "башня"
    },
    {
        eng: "frog",
        rus: "жаба"
    },
    {
        eng: "leaf",
        rus: "листок"
    },
    {
        eng: "tree",
        rus: "дерево"
    },
    {
        eng: "root",
        rus: "корень"
    },
    {
        eng: "web",
        rus: "паутина"
    },
    {
        eng: "polite",
        rus: "вежливый"
    },
    {
        eng: "deliberately",
        rus: "нарочно"
    },

];

export default wordList;