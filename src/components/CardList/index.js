import React from 'react';
import Card from "../Card";
import s from './CardList.module.scss'

const CardList = ({items}) => {
    return (
        <div className={s.Container}>
            {
                items.map(({eng, rus}) => <Card eng={eng} rus={rus}/>)
            }
        </div>
    );
};

export default CardList;