import React, {Component} from 'react';
import s from './Card.module.scss';
import cl from 'classnames';

class Card extends Component {
    state = {
        flipped: false,
    }

    handleClick = () => {
        this.setState(({flipped}) => {
            return {flipped: !flipped}
        });
    }

    render() {
        const {eng, rus} = this.props;
        const {flipped} = this.state;
        return (
            <div onClick={this.handleClick} className={cl(s.card, {[s.flip]: flipped})}>
                <div className={s.cardInner}>
                    <div className={s.cardFront}>
                        {eng}
                    </div>
                    <div className={s.cardBack}>
                        {rus}
                    </div>
                </div>
            </div>
        );
    }
}

export default Card;