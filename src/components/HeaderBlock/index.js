import React from "react";
// import {LikeOutlined} from "@ant-design/icons";
import s from './HeaderBlock.module.css'

const HeaderBlock = ({LogoImage, LogoLabel, animate = false}) => {
    return <div>
        <nav className={s.Navigation}>
            <img className={animate ? s.LogoImageA : null} src={LogoImage} alt={LogoLabel}/>
            <label className={s.Label}>
                <span className={s.React}>React</span>
                <span className={s.Marathon}>marathon</span>
                {/*<LikeOutlined className={s.Like}/>*/}
            </label>
        </nav>
    </div>
}

export default HeaderBlock