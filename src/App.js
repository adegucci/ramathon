import React from 'react';
import logo from './logo.svg';
import './App.css';
import ContentBlock from "./components/ContentBlock";
import FooterBlock from "./components/FooterBlock";
import HeaderBlock from "./components/HeaderBlock";
import wordList from "./data/words";
import Title from "./components/Title";
import Paragraph from "./components/Paragraph";
import CardList from "./components/CardList";

function App() {
    return (<>
            <HeaderBlock LogoImage={logo} animate LogoLabel="React"/>
            <ContentBlock showImage>
                <Title>Учите слова онлайн</Title>
                <Paragraph>Воспользуйтесь карточками для запоминания и пополнения активных словарных
                    запасов</Paragraph>
            </ContentBlock>
            <ContentBlock>
                <Title>Поехали!</Title>
                <CardList items={wordList}/>
            </ContentBlock>
            <FooterBlock year={2020}/>
        </>
    );
}

export default App;
