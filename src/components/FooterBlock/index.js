import React from "react";
import './FooterBlock.css'

const FooterBlock = (props) => {
    return (
        <div className={"Copyright"}>
        <span className={"CopyrightText"}>
            <span className={"SecondaryText"}>Made by</span> Alforno DeGucci <span
            className={"SecondaryText"}>| {props.year}</span>
        </span>
        </div>
    )
}
export default FooterBlock